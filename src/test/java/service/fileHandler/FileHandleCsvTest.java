package service.fileHandler;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class FileHandleCsvTest {
    FileHandleCsv fileHandling = new FileHandleCsv();

    @Test
    void readFile_success() {
        fileHandling.setFileLocation("C://tmp/direktori/data_sekolah.csv");
        Assertions.assertDoesNotThrow(() -> fileHandling.readFile());
    }
}