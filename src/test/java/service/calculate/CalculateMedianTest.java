package service.calculate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;


//nilai tengah
class CalculateMedianTest {

    List<Integer> listDataMedian = Arrays.asList(29, 19, 21, 52, 91, 50, 82, 65, 53, 84, 51, 90, 93);
//        List<Integer> listData = Arrays.asList(1, 5, 2, 2, 6, 5, 4);

    @Test
    @DisplayName("Calculate Median")
    void calculate_median() {
        CalculateMedian median = new CalculateMedian();
        double hasilMedian = median.calculate(listDataMedian);
        Assertions.assertEquals(53, hasilMedian);
    }

    @Test
    @DisplayName("Calculate Median Null")
    void calculate_medianNull() {
        CalculateMedian median = new CalculateMedian();
        double hasilMedian = median.calculate(null);
        Assertions.assertEquals(0, hasilMedian);

    }
}
