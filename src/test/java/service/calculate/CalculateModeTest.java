package service.calculate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;


class CalculateModeTest {

    List<Integer> listDataMode = Arrays.asList(29, 19, 21, 52, 91, 50, 82, 65, 53, 52, 51, 90, 93);

    @Test
    @DisplayName("Calculate Mode")
    void calculate_mode() {
        CalculateMode mode = new CalculateMode();
        double hasilMode = mode.calculate(listDataMode);
        Assertions.assertEquals(52, hasilMode);
    }

    @Test
    @DisplayName("Calculate Mode")
    void calculate_mode_null() {
        CalculateMode mode = new CalculateMode();
        double hasilMode = mode.calculate(null);
        Assertions.assertEquals(0, hasilMode);

    }
}