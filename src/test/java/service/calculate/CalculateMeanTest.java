package service.calculate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;


class CalculateMeanTest {
    List<Integer> listDataMean = Arrays.asList(89,89,89,89,90,90,91,91,92,93,93,94,94,96,96,98,98,100);

    @Test
    @DisplayName("Calculate Mean")
    void calculate_mean() {
        CalculateMean mean = new CalculateMean();
        double hasilMean = mean.calculate(listDataMean);

        // mengubah format double menjadi 2 titik dibelakang koma
        String formatHasilMean =  new DecimalFormat("#.##").format(hasilMean);
        double hasilMeanCovert = Double.parseDouble(formatHasilMean.replaceAll(",","."));
//        System.out.println(lhasilMean);

        Assertions.assertEquals(92.89, hasilMeanCovert);
    }

    //    negative test case
    @Test
    @DisplayName("Calculate Mean Null")
    void calculate_mean_null() {
        CalculateMean mean = new CalculateMean();
        double hasilMean = mean.calculate(null);
        Assertions.assertEquals(0, hasilMean);
    }
}