package service.fileHandler;

public interface IFileHandle {

    void readFile();
    void generateFile();
}
