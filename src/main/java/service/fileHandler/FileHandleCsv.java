package service.fileHandler;

import lombok.Data;
import service.calculate.CalculateMean;
import service.calculate.CalculateMedian;
import service.calculate.CalculateMode;
import view.MenuError;

import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class FileHandleCsv implements IFileHandle {
    private String fileLocation;
    Map<Integer, Integer> dataModus = new HashMap();
    List<Integer> ListData = new ArrayList();
    private Integer Menu;

    public void readFile() {
        File file = new File(fileLocation);

        try {
            FileReader reader = new FileReader(file);
            BufferedReader br = new BufferedReader(reader);
            String line = "";
            String[] tempArr;


            while ((line = br.readLine()) != null) {
                tempArr = line.split(";");

                for (int i = 1; i < tempArr.length; i++) {
                    int count = dataModus.getOrDefault(Integer.parseInt(tempArr[i]), 0);
                    dataModus.put(Integer.parseInt(tempArr[i]), count + 1);
                    ListData.add(Integer.parseInt(tempArr[i]));
                }
            }
            setDataModus(dataModus);
            setListData(ListData);

            br.close();
        }catch (IOException ioe) {
            MenuError menu = new MenuError();
            menu.setMessage("File tidak ditemukan !!!\n" + ioe.getMessage());
            menu.showMenu();
            System.exit(-1);
        }
    }

    @Override
    public void generateFile() {
        if(Menu == 1){
            writeModus(getDataModus());
        }else if(Menu == 2){
            writeData(getListData());
        }else{
            writeModus(getDataModus());
            writeData(getListData());
        }
    }

    public void writeModus(Map<Integer, Integer> data) {
        readFile();
        try {
            File file = new File("C://tmp/direktori/data_sekolah_modus.txt");

            if (file.createNewFile()) {
                System.out.println("New file has been created!");
            }

            FileWriter writer = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(writer);

            bw.write("Berikut Hasil Pengolahan Nilai : ");
            bw.newLine();
            bw.newLine();
            bw.write("Nilai               |      Frekuensi");
            bw.newLine();
            bw.write("5                   |           " + data.get(5));
            bw.newLine();
            bw.write("6                   |           " + data.get(6));
            bw.newLine();
            bw.write("7                   |           " + data.get(7));
            bw.newLine();
            bw.write("8                   |           " + data.get(8));
            bw.newLine();
            bw.write("9                   |           " + data.get(9));
            bw.newLine();
            bw.write("10                  |           " + data.get(10));
            bw.newLine();
            bw.flush();
            bw.close();

            System.out.println("File frekuensi nilai  telah di generate di C://temp/direktori silahkan cek");

        } catch (IOException e) {
            MenuError menu = new MenuError();
            menu.setMessage("terjadi kesalahan !!!\n" + e.getMessage());
            menu.showMenu();
            System.exit(-1);
        }
    }


    public void writeData(List<Integer> listData) {
        readFile();

        try {
            File file = new File("C://tmp/direktori/data_sekolah_modus_median.txt");
            if (file.createNewFile()) {
                System.out.println("New file has been created!");
            }
            FileWriter writer = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(writer);
            bw.write("Berikut Hasil Pengolahan Nilai : ");
            bw.newLine();
            bw.newLine();
            bw.write("Berikut Hasil sebaran data Nilai : ");
            bw.newLine();
            bw.write("Mean                 :           " + new DecimalFormat("#.##").format(new CalculateMean().calculate(listData)));
            bw.newLine();
            bw.write("Median               :           " + new DecimalFormat("#.##").format(new CalculateMedian().calculate(listData)));
            bw.newLine();
            bw.write("Mode                 :           " + new DecimalFormat("#.##").format(new CalculateMode().calculate(listData)));
            bw.newLine();
            bw.flush();
            bw.close();

            System.out.println("File Data telah di generate di C://temp/direktori silahkan cek");

        } catch (IOException e) {
            MenuError menu = new MenuError();
            menu.setMessage("terjadi kesalahan !!!\n" + e.getMessage());
            menu.showMenu();
            System.exit(-1);
        }
    }

}
