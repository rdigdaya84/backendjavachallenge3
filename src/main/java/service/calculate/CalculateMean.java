package service.calculate;

import java.util.List;


public class CalculateMean implements ICalculate {

    @Override
    public double calculate(List<Integer> listData) {
        try {
/*
            double sum = 0;
            for (Integer isi : listData) {
                sum += isi;
            }
            return sum / listData.size();
*/
            return (float) listData.stream().reduce(0, Integer::sum) / listData.size();

        } catch (NullPointerException | NumberFormatException ignored) {

        }
        return 0;
    }

}
