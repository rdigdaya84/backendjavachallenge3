package service.calculate;

import java.util.Collections;
import java.util.List;

//nilai tengah
public class CalculateMedian implements ICalculate {
    @Override
    public double calculate(List<Integer> listData) {
        try {
            Collections.sort(listData);
//
//            System.out.println(listData);
//            System.out.println(listData.size());

            if(listData.size() % 2 != 0)    //ganjil
            {
                return listData.get(listData.size() / 2);
            }else{
                return (double) (listData.get((listData.size()/ 2)-1) + listData.get((listData.size()/ 2))) / 2;
            }

        } catch (NullPointerException | NumberFormatException ignored) {

        }
        return 0;
    }
}
