package service.calculate;

import java.util.Collections;
import java.util.List;

public class CalculateMode implements ICalculate {
    @Override
    public double calculate(List<Integer> listData) {
        try {
            int mode, freqMode, temp, tempFreq;
            mode = temp = -1;
            freqMode = tempFreq = 0;
            Collections.sort(listData);

            for (Integer integer : listData) {
                if (mode == -1) {
                    mode = integer;
                    freqMode = 1;
                } else if (mode == integer) {
                    freqMode++;
                } else if (temp != integer) {
                    temp = integer;
                    tempFreq = 1;
                } else if (tempFreq >= freqMode) {
                    mode = integer;
                    freqMode = tempFreq + 1;
                    temp = -1;
                    tempFreq = 0;
                } else {
                    tempFreq++;
                }
            }
            return mode;
        } catch (NullPointerException | NumberFormatException ignored) {

        }
        return 0;
    }
}
