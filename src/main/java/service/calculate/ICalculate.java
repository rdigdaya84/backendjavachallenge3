package service.calculate;

import java.util.List;

public interface ICalculate {
    double calculate(List<Integer> listData);
}
