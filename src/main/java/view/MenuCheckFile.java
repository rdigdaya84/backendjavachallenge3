package view;

import java.io.File;
import java.util.Scanner;

public class MenuCheckFile extends Menu {
//    static String namaFile = null;

    @Override
    public void body() {
        System.out.println("Letakan file csv di direktori berikut: c://temp/direktori");
    }

    @Override
    public void selectOption() {
        Scanner input = new Scanner(System.in);
        System.out.print("\nCheck File kamu : (nama_filemu.csv) : ");

        String inputNamaFile = input.nextLine();
        File file = new File("C://tmp/direktori/" + inputNamaFile);
        if (!file.exists()) {
            MenuError menu = new MenuErrorCheckFile();
            menu.setMessage("File kamu tidak ditemukan !!\nkamu mungkin salah menginputkan :(");
            menu.showMenu();
            System.exit(-1);
        } else {
            System.out.println("\nFile Kamu Sudah Terverifikasi.\n");
            System.out.print("tekan Enter untuk Melanjutkan...");
            input.nextLine();

            MainMenu menu = new MainMenu();
            menu.setNamaFile(inputNamaFile);
            menu.showMenu();
        }
    }
}
