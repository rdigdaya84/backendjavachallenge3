package view;

import lombok.Data;
import service.fileHandler.FileHandleCsv;

import java.util.InputMismatchException;
import java.util.Scanner;
@Data
public class MainMenu extends Menu {
    String namaFile;

    public void body() {
        System.out.println("\nPilih Menu : ");
        System.out.println("1. Generate txt untuk menampilkan frekuensi nilai ");
        System.out.println("2. Generate txt untuk menampilkan nilai Modus, Median, Mean");
        System.out.println("3. Generate kedua Pilihan 1 & 2");
        System.out.println("0. Tutup Aplikasi\n");
        System.out.println("---------------------------------");
    }

    public void selectOption() {

//        String namaFile = "data_sekolah.csv";
//        String namaFile = "data_erroor.csv";

        FileHandleCsv fileHandle = new FileHandleCsv();
        fileHandle.setFileLocation("C://tmp/direktori/" + getNamaFile());

        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan Pilihanmu : ");

        try {
            int inputMenu = input.nextInt();
            if (inputMenu > 0 && inputMenu < 4) {
                fileHandle.setMenu(inputMenu);
                fileHandle.generateFile();
                backToMainMenu();
            } else if(inputMenu == 0) {
                System.out.println("Terimakasih telah menggunakan aplikasi kami :)");
            } else{
                MenuError menu = new MenuError();
                menu.setMessage("Pilihan yang kamu masukkan \ntidak tersedia !!!");
                menu.showMenu();
            }
        } catch (InputMismatchException e) {
            MenuError menu = new MenuError();
            menu.setMessage("Yang Kamu masukkan Bukan Angka !!!\n");
            menu.showMenu();
            System.exit(-1);
        }catch (NumberFormatException e) {
            MenuError menu = new MenuError();
            menu.setMessage("Ada kesalahan Data,\npada file yang kamu gunakanan !!!\n" + e.getMessage());
            menu.showMenu();
            System.exit(-1);
        }
    }

    void backToMainMenu() {
        System.out.print("\ntekan Enter untuk Kembali ke menu ");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();

        Menu menu = new MainMenu();
        menu.showMenu();
    }
}
