package view;

import java.util.InputMismatchException;
import java.util.Scanner;

public class MenuErrorCheckFile extends MenuError {

    @Override
    public void selectOption() {
        System.out.println("---------------------------------");
        System.out.print("\ntekan Enter untuk Melanjutkan... ");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();

        System.out.println("---------------------------------");
        System.out.println("Pilih Menu : ");
        System.out.println("1. Kembali Ke Menu Awal");
        System.out.println("0. Tutup Aplikasi");
        System.out.println("---------------------------------");


        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan Pilihanmu : ");
        try {
            int inputMenu = input.nextInt();
            if (inputMenu == 1) {
                Menu menu = new MenuCheckFile();
                menu.showMenu();
            } else {
                System.out.println("Terimakasih telah menggunakan aplikasi kami :)");
                System.exit(0);
            }
        } catch (InputMismatchException|NumberFormatException  e) {
            MenuError menu = new MenuErrorCheckFile();
            menu.setMessage("Yang Kamu masukkan Bukan Angka !!!\n");
            menu.showMenu();
            System.exit(-1);
        }
    }
}
