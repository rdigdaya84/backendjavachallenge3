package view;

import lombok.Data;

import java.util.Scanner;

@Data
public class MenuError extends Menu{
    String message;

    @Override
    public void header() {
        System.out.println("---------------------------------");
        System.out.println("             ERROR               ");
        System.out.println("---------------------------------");
    }

    @Override
    public void body() {
        System.out.println("\n"+getMessage()+"\n");
    }

    @Override
    public void selectOption() {
        System.out.println("---------------------------------");
        System.out.print("\ntekan Enter untuk Kembali ke menu ");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();

        Menu menu = new MainMenu();
        menu.showMenu();
    }

}
